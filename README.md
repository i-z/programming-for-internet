# README #

### What is this repository for? ###

* Programming for Internet course project
* Technical University of Sofia, branch Plovdiv
* Nov 2016 - Jan 2017 

### How do I get set up? ###

* Clone the repository to a local folder
* Start localhost service (WAMP) or IIS (point Frontend folder as Physical Path) and run the backend solution
* Dependencies - Visual Studio 2015 or newer, Microsoft SQL Server 2014 Express or newer
* Database configuration - right click on "DataPopulation.publish.xml" in back-end project "DataPopulation" and then click "Publish"

### Who do I talk to? ###

Ivan Zdravkov - ivanzdravkovbg@gmail.com

Trifon Dardzhonov - trifon.dardzhonov@gmail.com