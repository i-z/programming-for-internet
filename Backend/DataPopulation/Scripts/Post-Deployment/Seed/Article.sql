﻿SET IDENTITY_INSERT [dbo].[Article] ON

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (1, N'Test Article 1', N'Test Article Content', NULL, 1, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-04', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (2, N'Test Article 2', N'Test Article Content', NULL, 1, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-10', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (3, N'Test Article 3', N'Test Article Content', NULL, 1, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-02-01', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (4, N'Test Article 4', N'Test Article Content', NULL, 1, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-02-10', NULL, NULL) 


INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (5, N'Test Article 5', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-04', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (6, N'Test Article 6', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-05', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (7, N'Test Article 7', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-06', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (8, N'Test Article 8', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-07', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (9, N'Test Article 9', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-01-08', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (10, N'Test Article 10', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-02-01', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (11, N'Test Article 11', N'Test Article Content', NULL, 2, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-02-02', NULL, NULL) 

INSERT [dbo].[Article] ([Id], [Name], [Text], [Image], [CategoryID], [StatusID], [CreatedBy], [CreatedAt], [LastModifiedBy], [LastModifiedAt]) 
	VALUES (12, N'Test Article 12', N'Test Article Content', NULL, 1, 1, N'633d03e5-86fb-4b61-9fec-25fffff534b7', '2017-02-03', NULL, NULL) 

SET IDENTITY_INSERT [dbo].[Article] OFF
